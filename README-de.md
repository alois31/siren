Deutsch [English](README.md)

# Überblick

Sirene ist eine einfache, freie Alarmierungs-App. Alarmnachrichten werden per SMS empfangen.

Manche Notfalleinrichtungen (z.B. Feuerwehren, Bergwachten) benutzen zentrale Alarmierungsgeräte für SMS-basierte Alarmierung anstelle von oder zusätzlich zu traditionellen Alarmierungsmethoden wie Piepsern oder Sirenen.
Leider erschwert das die Unterscheidung zwischen Alarmen und störenden Nachrichten.
Ferner werden Alarme nicht bemerkt, wenn das Telefon stummgeschaltet ist oder sich im Nachtmodus (auch bekannt als "Nicht stören"-Modus) befindet.
Diese Anwendung behebt beide Probleme, indem unabhängig von den meisten Geräteeinstellungen deutliche Alarme zur Verfügung gestellt werden, wenn SMS-Nachrichten von einem konfigurierten Alarmierungsgerät empfangen werden.

# Funktionen

* Fähigkeit, mehrere Alarmierungsgeräte hinzuzufügen

* Deutlicher Alarm, unabhängig von geräteweiter Stummschaltung und vom Nachtmodus:

    * Lauter Sirenenton, unabhängig von der geräteweiten Lautstärke (kann innerhalb der Anwendung ausgeschaltet werden)
    
    * Characteristisches Vibrationsmuster
    
    * Anzeige der Alarmnachricht mitsamt dem Namen der Einrichtung, ohne das Telefon entsperren zu müssen

* Keine Werbung, In-App-Käufe, Tracking, Registrierung oder sonstiger unnötiger oder proprietärer Ballast

# Installation und Einrichtung

Bevor Sie diese App installieren können, müssen Sie ein Installationspaket (APK-Datei) erhalten.
Sie können dieses aus dem Quelltext erstellen oder ein Binärpaket vom Autor anfordern.
Die Lizenz erlaubt die Verteilung des Installationspaketes, welches durch eine dieser Methoden erhalten wurde.
Sie dürfen das Installationspaket also auch weitergeben.

## Erstellung aus dem Quelltext

Diese Methode ist für fortgeschrittene Benutzer.
Sie müssen mit Android-Entwicklung und der Kommandozeile vertraut sein.
Ein dazu benötigtes Programm, Android Studio, sowie dessen Installationsanleitung sind nur auf Englisch verfügbar.

1. Laden Sie [Android Studio](https://developer.android.com/studio/) herunter und installieren Sie es auf Ihrem Computer.

2. Laden Sie den Quelltext herunter:

        git clone git@gitlab.com:alois31/siren.git

   Sie können auch die Funktion "Get from Version Control" in Android Studio benutzen.

3. Öffnen Sie das Projekt in Android Studio.

4. Erzeugen Sie eine signierte APK-Datei in Android Studio.
   Dies ist im Menü verfügbar (Build > Generate Signed Bundle / APK).

    1. Wählen Sie "APK" auf der ersten Seite des Assistenten.

    2. Folgen Sie der zweiten Seite des Assistenten.

    3. Auf der dritten Seite müssen Sie "release" auswählen und beide Signaturversionen aktivieren, da das Installationspaket ansonsten unter Umständen nicht auf allen Telefonen funktioniert.

    4. Klicken Sie auf "Finish" und warten Sie, bis die Erstellung abgeschlossen ist.
       Das Installationspaket sollte als ```app/release/app-release.apk``` verfügbar sein.

   Sie können das Installationspaket nun mit Ihrer bevorzugten Methode auf das Telefon übertragen oder es an andere weitergeben.

## Herunterladen eines Binärpakets

Die einfachere Möglichkeit ist es, ein Binärpaket zu erhalten (d.h. jemand anders hat die APK-Datei für Sie aus dem Quelltext erstellt).
Vom Autor erstellte Binärpakete sind unter [https://gitlab.com/alois31/siren-bin](https://gitlab.com/alois31/siren-bin) verfügbar.

## Weitergeben des Installationspakets

Sie dürfen die Anwendung weitergeben und verteilen, solange die Bedingungen der Lizenz eingehalten werden.
Insbesondere werden garantiert alle Lizenzbestimmungen eingehalten, wenn Sie ein Installationspaket durch eine der obigen beiden Methoden erhalten haben und dieses weitergeben, falls Sie keine Änderungen vorgenommen haben und dem Empfänger keine zusätzlichen Bestimmungen auferlegen.

## Installation

Sobald Sie eine APK-Datei auf Ihrem Telefon haben, können Sie diese installieren.
Finden Sie die APK-Datei im Dateimanager Ihres Telefons und tippen Sie sie an.
Es könnte sein, dass Sie eine Sicherheitswarnung bezüglich der Installation aus unbekannten Quellen erhalten.
In diesem Fall müssen Sie die Installation aus unbekannten Quellen in den Geräteeinstellungen erlauben (die Sicherheitswarnung gibt Ihnen möglicherweise bereits die Anleitung dazu).
Tippen Sie auf "Installieren".
Nach dem Abschluss der Installation finden Sie die Anwendung in der Anwendungsübersicht.

## Einrichtung

Nach der Installation müssen Sie das Alarmierungsgerät zuerst hinzufügen.

1. Öffnen Sie die Anwendung.
   Wenn Sie die Anwendung das erste Mal öffnen (oder die Berechtigung davor abgelehnt haben), wird Sie die SMS-Berechtigung anfordern.
   Sie müssen diese Berechtigung gewähren, da die App ansonsten funktionslos ist.
   Entgegen des Anscheins der Anfrage wird (und kann) die Anwendung keine SMS-Nachrichten versenden.

2. Tippen Sie auf den "+"-Knopf in der rechten unteren Ecke.
   Geben Sie den Namen der Notfalleinrichtung (dieser wird dann auf Alarmen angezeigt) und die eingehende Nummer ein.
   Es wird empfohlen, die Ländervorwahl mit anzugeben.
   Tippen Sie auf "hinzufügen".
   Falls nötig, wiederholen Sie diesen Vorgang für andere Einrichtungen.

3. Prüfen Sie unten, ob eine gerätespezifische Einrichtung erforderlich ist (ansonsten können Alarme u.U. nicht erhalten werden).
   Die Anwendung ist jetzt bereit.

## Gerätespezifische Einrichtung

Manche Gerätehersteller sind bekannt dafür, aggressive Strategien einzusetzen, die Anwendungen daran hindern sollen, Ihren Akku zu entleeren.
Diese Anwendung wird Ihren Akku nicht entleeren, aber manche dieser Strategien können ihre normale Funktionalität behindern.

Sollte Ihr Gerätehersteller unten nicht aufgelistet sein und Sie wissen, dass dieser solche Strategien einsetzt, oder falls Sie keine Alarme erhalten, so erstellen Sie bitte einen Problembericht ("issue") oder kontaktieren Sie den Autor der Anwendung.
Das gilt auch, falls der Hersteller zwar unten aufgeführt ist, aber das Vorgehen nicht funktioniert.

### Huawei

Huawei-Geräte "beenden" Hintergrundanwendungen, besonders wenn der Bildschirm ausgeschaltet ist.
Eine Folge des "Beendens" ist, dass SMS-Nachrichten durch die Anwendung nicht empfangen werden.
Sie können dieses Verhalten pro Anwendung deaktivieren, indem Sie sie zur Liste der "Geschützten Apps" hinzufügen.
Der genaue Ort dieser Einstellungen hängt von der EMUI-Version ab, aber zu bekannten Pfaden gehören:

* Einstellungen > Einstellungen > Akkumanager > Geschützte Apps

* Einstellungen > Akku > Geschützte Apps

# Fehlerbehandlung

* Anwendung kann nicht installiert werden.

    * Überprüfen Sie Ihre Android-Version. Die Anwendung funktioniert nur mit IceCreamSandwich (4.0) oder höher. Außer wenn Ihr Telefon sehr alt ist, ist die Android-Version sehr wahrscheinlich neu genug.

    * Überprüfen Sie, ob Sie die Installation aus unbekannten Quellen in den Geräteeinstellungen zugelassen haben.

    * Ansonsten erstellen Sie einen Problembericht ("issue") oder kontaktieren den Autor.

* Alarm funktioniert nich.

    * Überprüfen Sie, ob die Alarm-Nachricht in der Nachrichten-Anwendung angezeigt wird. Falls nicht, stellen Sie sicher, dass Ihre Telefonnummer im Alarmierungsgerät registriert ist und Ihr Telefon sich nicht im Flugmodus befindet.

    * Überprüfen Sie, ob die Anwendung die SMS-Berechtigung besitzt.

    * Überprüfen Sie, ob die korrekte eingehende Nummer in der Anwendung registriert ist.

    * Überprüfen Sie, ob Sie die gerätespezifische Einrichtung nach obiger Anleitung durchgeführt haben.

    * Ansonsten erstellen Sie einen Problembericht ("issue") oder kontaktieren den Autor.

* Auf irgendeine andere Weise nicht mit der Anwendung zufrieden.

    * Erstellen Sie einen Problembericht ("issue") oder kontaktieren Sie den Autor.

# Kontakt

Der Autor ist [Alois Wohlschlager](mailto:alois1@gmx-topmail.de).
Sie können mich per E-Mail kontaktieren, indem Sie auf den Verweis klicken.

# Lizenz und Haftungsausschluss

Sämtlicher Quelltext ist von der [Apache License, Version 2.0](LICENSE-Apache-2.0.txt) abgedeckt.
Für den Sirenenton gilt eine andere Lizenz, die [Creative Commons Attribution ShareAlike 3.0 Unported](LICENSE-CC-BY-SA-3.0.txt).

Es gibt keine Garantie oder Gewährleistung jeglicher Art.
Der Autor wird nicht für Schäden haften, die durch die Installation oder Verwendung der Anwendung entstehen, inklusive aber nicht beschränkt auf Schäden, die durch verpasste Alarme entstehen.
Für weitere Informationen siehe die Apache License, Version 2.0.
