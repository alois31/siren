package com.gitlab.alois31.siren;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Pair;
import android.widget.ArrayAdapter;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Map;

class AlertsBackend {
    private ArrayList<String> store;
    private SharedPreferences backend;

    private void recreateStore() {
        store.clear();
        store.addAll(backend.getAll().keySet());
    }

    AlertsBackend(SharedPreferences backend) {
        store = new ArrayList<>();
        this.backend = backend;
        recreateStore();
    }

    ArrayAdapter<String> createAdapter(@NonNull Context context, @LayoutRes int resource) {
        return new ArrayAdapter<>(context, resource, store);
    }

    void add(String facility, String incoming_id) {
        backend.edit().putString(facility, incoming_id).apply();
        recreateStore();
    }

    Pair<String, String> get(int id) {
        String facility = store.get(id);
        String incoming_id = backend.getString(facility, null);
        return new Pair<>(facility, incoming_id);
    }

    void delete(String facility) {
        backend.edit().remove(facility).apply();
        recreateStore();
    }

    Map<String, ?> getAll() {
        return backend.getAll();
    }
}
