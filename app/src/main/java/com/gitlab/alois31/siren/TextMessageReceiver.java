package com.gitlab.alois31.siren;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.Map;

public class TextMessageReceiver extends BroadcastReceiver {
    private void alarm(Context context, String facility, String message) {
        Intent launch = new Intent(context, AlertActivity.class);
        launch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        launch.putExtra("facility", facility);
        launch.putExtra("message", message);
        context.startActivity(launch);
    }

    private void spuriousTrigger() {
        Log.w("Siren", "SMS broadcast receiver triggered spuriously");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals("android.provider.Telephony.SMS_RECEIVED")) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                if (pdus != null) {
                    if (pdus.length > 0) {
                        String sender = SmsMessage.createFromPdu((byte[]) pdus[0]).getOriginatingAddress();
                        AlertsBackend backend = new AlertsBackend(context.getApplicationContext().getSharedPreferences("com.gitlab.alois31.siren.alerts", Context.MODE_PRIVATE));
                        Map<String, ?> map = backend.getAll();
                        for (Map.Entry<String, ?> entry : map.entrySet()) {
                            String id = entry.getValue().toString();
                            if (PhoneNumberUtils.compare(id, sender)) {
                                StringBuilder message = new StringBuilder();
                                for (Object pdu : pdus) {
                                    message.append(SmsMessage.createFromPdu((byte[]) pdu).getMessageBody());
                                }
                                alarm(context, entry.getKey(), message.toString());
                            }
                        }
                    } else {
                        spuriousTrigger();
                    }
                } else {
                    spuriousTrigger();
                }
            } else {
                spuriousTrigger();
            }
        } else {
            spuriousTrigger();
        }
    }
}
