package com.gitlab.alois31.siren;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class AlertActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            extras = Bundle.EMPTY;
        }
        String facility = extras.getString("facility");
        String message = extras.getString("message");

        TextView alarm_facility = findViewById(R.id.alert_facility);
        TextView alarm_message = findViewById(R.id.alert_message);
        alarm_facility.setText(facility);
        alarm_message.setText(message);

        SharedPreferences settings = getApplicationContext().getSharedPreferences("com.gitlab.alois31.siren.settings", Context.MODE_PRIVATE);
        boolean quiet = settings.getBoolean("quiet", false);

        if (quiet) {
            final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (vibrator != null) {
                long[] pattern = {0, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000};
                vibrator.vibrate(pattern, -1);
            }

            Button stop_alarm = findViewById(R.id.stop_alert);
            stop_alarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (vibrator != null) {
                        vibrator.cancel();
                    }
                    AlertActivity.this.finish();
                }
            });
        } else {
            final AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
            final int volume = (audioManager != null) ? audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) : 0;
            int maxVolume = (audioManager != null) ? audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) : 0;
            if (audioManager != null) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);
            }
            final MediaPlayer player = MediaPlayer.create(this, R.raw.sirene);
            player.start();
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    player.release();
                    if (audioManager != null) {
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
                    }
                }
            });

            final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (vibrator != null) {
                long[] pattern = {0, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000};
                vibrator.vibrate(pattern, -1);
            }

            Button stop_alarm = findViewById(R.id.stop_alert);
            stop_alarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    player.stop();
                    player.release();
                    if (audioManager != null)
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0);
                    if (vibrator != null) {
                        vibrator.cancel();
                    }
                    AlertActivity.this.finish();
                }
            });
        }
    }
}
