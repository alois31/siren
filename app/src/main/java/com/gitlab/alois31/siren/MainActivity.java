package com.gitlab.alois31.siren;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ListView alerts = findViewById(R.id.alerts);
        final AlertsBackend backend = new AlertsBackend(getApplicationContext().getSharedPreferences("com.gitlab.alois31.siren.alerts", Context.MODE_PRIVATE));
        final ArrayAdapter<String> adapter = backend.createAdapter(this, android.R.layout.simple_list_item_1);
        alerts.setAdapter(adapter);
        alerts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                final Pair<String, String> entry = backend.get(i);
                builder.setTitle(entry.first);
                builder.setMessage(entry.second);
                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        backend.delete(entry.first);
                        adapter.notifyDataSetChanged();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });

        final LayoutInflater inflater = this.getLayoutInflater();
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.add_alert);
                @SuppressLint("InflateParams")
                View content = inflater.inflate(R.layout.add_alert_dialog, null);
                builder.setView(content);
                final EditText facility = content.findViewById(R.id.facility);
                final EditText incoming_id = content.findViewById(R.id.incoming_id);
                builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        backend.add(facility.getText().toString(), incoming_id.getText().toString());
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton(R.string.cancel, null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.permission_notice);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECEIVE_SMS}, 0);
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS}, 0);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.overlay_notice);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                    startActivity(intent);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SharedPreferences settings = getApplicationContext().getSharedPreferences("com.gitlab.alois31.siren.settings", Context.MODE_PRIVATE);
        MenuItem quietBox = menu.findItem(R.id.action_quiet);
        boolean quiet = settings.getBoolean("quiet", false);
        quietBox.setChecked(quiet);
        if (quiet) {
            quietBox.setIcon(android.R.drawable.ic_lock_silent_mode);
        } else {
            quietBox.setIcon(android.R.drawable.ic_lock_silent_mode_off);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        SharedPreferences settings = getApplicationContext().getSharedPreferences("com.gitlab.alois31.siren.settings", Context.MODE_PRIVATE);

        if (id == R.id.action_test) {
            Intent launch = new Intent(MainActivity.this, AlertActivity.class);
            launch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launch.putExtra("facility", getResources().getString(R.string.test_alert));
            launch.putExtra("message", getResources().getString(R.string.test_alert));
            startActivity(launch);
            return true;
        } else if (id == R.id.action_quiet) {
            if (item.isChecked()) {
                item.setChecked(false);
                settings.edit().putBoolean("quiet", false).apply();
                item.setIcon(android.R.drawable.ic_lock_silent_mode_off);
            } else {
                item.setChecked(true);
                settings.edit().putBoolean("quiet", true).apply();
                item.setIcon(android.R.drawable.ic_lock_silent_mode);
            }
            return true;
        } else if (id == R.id.action_about) {
            Intent launch = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(launch);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
