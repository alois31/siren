[Deutsch](README-de.md) English

# Overview

Siren is a simple, free alerting app. It receives alert messages via SMS.

Some emergency facilities (e.g. firefighters, mountain rescue) use central alerting devices for SMS-based alerting instead of or in addition to traditional alerting mechanisms such as beepers or sirens.
Unfortunately, this makes it harder to distinguish between alerts and annoyances, and alerts will not be noticed when the phone is muted or in night mode (also known as "Do not disturb" mode).
This app fixes both problems, by providing noticeable alerts when receiving SMS messages from any configured alerting device, independent of most device settings.

# Features

* Ability to add multiple alerting devices

* Very noticeable alert, independent of device mute and night mode settings:

    * Loud siren sound, independent of device volume (can be turned off inside the app)
   
    * Characteristic vibration pattern
   
    * Display of the alert message, along with the facility name, without needing to unlock the phone

* No advertisements, in-app purchases, tracking, registration or other useless or proprietary garbage

# Installation and setup

Before you can install this app, you need to obtain an installation package (APK file) first.
You can do this by building from source, or by requesting a binary package from the author.
The license permits distribution of the installation package obtained by either method, so you may also share the installation package.

## Building from source

This method is for advanced users.
You need to be familiar with Android development and the command line.

1. Download and install [Android Studio](https://developer.android.com/studio/) on your computer	.

2. Obtain the source code:

        git clone git@gitlab.com:alois31/siren.git

   You can also use the "Get from Version Control" functionality in Android Studio.

3. Open the project in Android Studio.

4. In Android Studio, generate a signed APK file.
   This is available on the menu (Build > Generate Signed Bundle / APK).

    1. Select "APK" on the first page on the wizard.

    2. Follow the second page of the wizard.

    3. On the third page, you need to select "release" and enable both signature versions, otherwise the installation package may not work on all phones.

    4. Click "Finish" and wait for the build to finish.
       The installation package should be available as ```app/release/app-release.apk```.

   You can now upload the installation package to your phone by your preferred method, or share it with others.
 
## Downloading a binary package

The easier method is to obtain a binary package (that is, someone else built the APK file from source for you).
Binary release packages built by the author are available at [https://gitlab.com/alois31/siren-bin](https://gitlab.com/alois31/siren-bin).

## Sharing the installation package

You may share and distribute the app, as long as the requirements of the license are satisfied.
In particular, sharing any installation package obtained by either of the methods above is guaranteed to fulfil all requirements if you did not make any changes and do not impose any additional terms on the recipient.

## Installation

Once you have an APK file on your phone, you can install it.
Locate the APK file in your phone's file manager and tap on it.
You may get a security warning regarding installation from unknown sources.
In this case, you need to allow installation from unknown sources in the device settings (the security warning may already instruct you how to do that).
Tap on "Install".
After the installation is finished, you will find the app in the app drawer.

## Setup

After the installation, you need to add the alerting device first.

1. Open the app.
   When opening the app for the first time (or if you denied the permission before), it will ask for SMS permisson.
   You need to allow this, as otherwise the app has no function.
   Unlike the prompt suggests, the app will not (and in fact cannot) send SMS messages.

2. Tap on the "+" button in the bottom right corner.
   Enter the name of the emergency facility (this will be shown on alerts) and the incoming ID of alert messages.
   It is recommended to include the country prefix.
   Tap "Add".
   Repeat this procedure for other facilities if required.

3. Check below if device-specific setup is required (otherwise, alerts might not be received).
   The app is ready now.

## Device-specific setup

Some device vendors are known to employ aggressive strategies to prevent apps from draining your battery.
This app will not drain your battery, but some of these strategies may interfere with its normal operation.

If your device vendor is not listed below, and you know that it employs such strategies or if you don't receive alerts, please report an issue or contact the author of the app.
This also applies if the vendor is listed below, but the procedure does not work.

### Huawei

Huawei devices "clear" background apps, particularly when the screen is off.
One consequence of "clearing" is that SMS messages will not be received by the app.
You can disable this behavior per-app by adding it to the list of "Protected apps".
The exact location of this setting depends on the EMUI version, but known paths include:

* Settings > Advanced Settings > Battery manager > Protected apps

* Settings > Battery > Protected apps

# Troubleshooting

* App cannot be installed.

    * Check your Android version. The app only works with IceCreamSandwich (4.0) and above. Unless your phone is very old, the Android version is very likely to be new enough.
    
    * Check whether you enabled installation from unknown sources in the device settings.
    
    * Otherwise, report an issue or contact the author.

* Alert does not work.

    * Check whether the alert message shows up in the messages app. If not, make sure your phone number is registered in the alerting device and the phone is not in airplane mode.

    * Check whether the app has SMS permission.

    * Check whether the correct incoming ID is registered in the app.

    * Check whether you followed the device-specific setup instructions above.

    * Otherwise, report an issue or contact the author.

* Not satisfied with the app in any other way.

    * Report an issue or contact the author.

# Contact

The author is [Alois Wohlschlager](mailto:alois1@gmx-topmail.de).
You can contact me via e-mail by clicking on the link.

# License and warranty disclaimer

All code is covered by the [Apache License, Version 2.0](LICENSE-Apache-2.0.txt).
The siren sound is governed by different terms, the [Creative Commons Attribution ShareAlike 3.0 Unported](LICENSE-CC-BY-SA-3.0.txt).

There is no warranty of any kind.
The author will not be responsible for any damages resulting from the installation or use of the app, including but not limited to damages resulting from missed alerts.
For further information, see the Apache License, Version 2.0.
